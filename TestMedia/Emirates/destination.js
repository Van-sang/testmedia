// DATA

let destinationData = (function(){
    let objetDestination = destination;

    return {
        getDestination: function(){
            return objetDestination;
        }
    }
})();


// UI

let destinationUI = (function(){
    let DomStrings = {
        test: '.test',
        arrowLeft: ['.arrowLeftLaguna', '.arrowLeftImpiana', '.arrowLeftGrand'],
        arrowRight: ['.arrowRightLaguna', '.arrowRightImpiana', '.arrowRightGrand'] 
    }

    let labels = {
        lagunaBeach: 'Laguna Beach Hotel & Spa',
        impiania: 'Impiana Resort Samui',
        grandArc: 'Grand Arc Hanzomon'
    }

    let template = function(destination){
    
    let cards =  
        `<div class='col-md-4 z'>
            <div class='section__destinationCard--minHeight'>
                <div class='section__destinationCard z'>
                    <div class='section__arrowsSlider ' %ArrowDisplay%>
                        <div class='section__arrowsSlider--position z'>
                            <div class='section__arrowsSlider--left arrowLeft${destination.labelCut}'><i class="fas fa-chevron-left arrow-slider"></i></div>
                            <div class='section__arrowsSlider--right arrowRight${destination.labelCut}'><i class="fas fa-chevron-right arrow-slider"></i></div>
                        </div>
                    </div>
                    <div class='section__arrowsSlider--pictures'>
                        <div class='section__arrowsSlider--pictureSideBySide sliderMove${destination.labelCut}'>
                            <div class='section__arrowsSlider--pictureOne'>
                                <img class='section__destinationCard--pictureSize z' src='${destination.link}' alt='?' />
                            </div>
                            <div class='section__arrowsSlider--pictureOne'>
                                <img class='section__destinationCard--pictureSize z' src='imgs/CLOS_DU_LITTORAL.png' alt='?' />
                            </div>
                            <div class='section__arrowsSlider--pictureOne'>
                                <img class='section__destinationCard--pictureSize z' src='imgs/LAGUNA_BEACH.png' alt='?' />
                            </div>
                        </div>
                    </div>
                    <div class='section__destinationCard--title z'>
                        <div class='section__destinationCard--title--country z'>${destination.place}</div>
                        <div class='section__destinationCard--title--price z'>${destination.upto}</div>
                    </div>
                </div>
                <div>
                    <div class='section__destinationCardDescription z'>
                        <div class='z section__destinationCardDescription--positionLeft'>
                            <div class='section__destinationCardDescription--country z'>${destination.country} - <span class='section__destinationCardDescription--city z'>${destination.place}</span></div>
                            <div class='section__destinationCardDescription--label z'>${destination.label} <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <div class='section__destinationCardDescription--premium z'>
                                <div class='section__destinationCardDescription--premium--class'>${destination.tags[0].label}</div>	
                                <div class='section__destinationCardDescription--premium--breakfast'>${destination.tags[1].label}</div>
                            </div>
                        </div>
                        <div class='z section__destinationCardDescription--button'>
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;

        let newCard;

        if (destination.label == labels.lagunaBeach || destination.label == labels.impiania || destination.label == labels.grandArc){
            newCard = cards;
        } else {
            newCard = cards.replace('%ArrowDisplay%', 'style="display:none";')
        }

        document.querySelector(DomStrings.test).insertAdjacentHTML('beforeend', newCard);
    }

    return {
        getDomStrings: function(){
            return DomStrings;
        },
        getTemplate: function(des){
            template(des);
        }
    }
})();



// CONTROLER DATA AND UI

let destinationController = (function(desData, desUI){

    // CREATE CARDS
    let objet = desData.getDestination();
    objet.forEach(function(cur, index){
        desUI.getTemplate(cur);
    })

    
    // SLIDER
    let allArrow = desUI.getDomStrings();

    let decalage = 0; 
    allArrow.arrowLeft.forEach(function(cur, index){
        
            document.querySelector(cur).addEventListener('click', function(event){
                decalage += 100;

                if (decalage <= 200){
                    document.querySelector('.sliderMove' + cur.slice(10)).style.left = `-${decalage}%`;
                } else {
                    decalage = 200;
                }
            });
    });

    allArrow.arrowRight.forEach(function(cur, index){
        

        document.querySelector(cur).addEventListener('click', function(event){
            decalage -= 100;

            if (decalage >= 0){
                document.querySelector('.sliderMove' + cur.slice(11)).style.left = `-${decalage}%`;
            } else {
                decalage = 0;
            }
        });
});

})(destinationData, destinationUI);

